class CartsController < ApplicationController
  skip_before_action :authorize, only: [:create, :update, :destroy]
  before_action :set_cart, only: [:show, :edit, :update, :destroy]
  rescue_from ActiveRecord::RecordNotFound, with: :invalid_cart
  respond_to :html, :json, :js

  def index
    @carts = Cart.all
  end

  def new
    @cart = Cart.new
  end

  def create
    @cart = Cart.new(cart_params)

    flash[:notice] = 'Cart was successfully created.' if @cart.save
    respond_with @cart
    # respond_to do |format|
      # if @cart.save
        # format.html { redirect_to @cart, notice: 'Cart was successfully created.' }
        # format.json { render :show, status: :created, location: @cart }
      # else
        # format.html { render :new }
        # format.json { render json: @cart.errors, status: :unprocessable_entity }
      # end
    # end
  end

  def update
    flash[:notice] = 'Cart was successfully updated.' if @cart.update(cart_params)
    respond_with @cart
    # respond_to do |format|
    #   if @cart.update(cart_params)
    #     format.html { redirect_to @cart, notice: 'Cart was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @cart }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @cart.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /carts/1
  # DELETE /carts/1.json
  def destroy
    @cart.destroy if @cart.id == session[:cart_id]
    session[:cart_id] = nil
    respond_with @cart #do |format|
      # format.js
      # format.html { redirect_to store_url }
      # format.json { head :no_content }
    # end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_cart
    @cart = Cart.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def cart_params
    params[:cart]
  end

  def invalid_cart
    logger.error "Attempt to access invalid cart #{ params[:id] }"
    redirect_to store_url, notice: 'Invalid cart'
  end

end
