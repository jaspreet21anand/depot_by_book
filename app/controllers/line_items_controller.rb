class LineItemsController < ApplicationController
  include CurrentCart
  skip_before_action :authorize, only: [:create, :destroy, :update]
  before_action :set_cart, only: [:create, :destroy, :update]
  before_action :set_line_item, only: [:show, :edit, :update, :destroy]

  def index
    @line_items = LineItem.all
  end

  def new
    @line_item = LineItem.new
  end

  def create
    product = Product.find(params[:product_id])
    @line_item = @cart.add_product(product)

    respond_to do |format|
      if @line_item.save
        format.html { redirect_to store_url }
        format.js { @current_item = @line_item }
        format.json { render :show, status: :created, location: @line_item }
      else
        format.html { render :new }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @line_item.update(quantity: @line_item.quantity + 1)
    @current_item = @line_item
    render 'create'
    # respond_to do |format|
    #   if @line_item.update(line_item_params)
    #     format.html { redirect_to @line_item, notice: 'Line item was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @line_item }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @line_item.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  def destroy
    if @line_item.quantity == 1
      @line_item.destroy
    else
      @line_item.update(quantity: @line_item.quantity - 1)
    end
    respond_to do |format|
      format.js do
        @current_item = @line_item
        if @cart.line_items.empty?
          render 'carts/destroy'
        else
          render 'create'
        end
      end
      if @line_item.cart_id
        # format.html { redirect_to Cart.find(@line_item.cart_id), notice: 'Item was successfully removed.' }
        format.html { redirect_to store_url }
      else
        format.html { redirect_to line_items_path, notice: 'Item was successfully removed.' }
      end
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_line_item
      @line_item = LineItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def line_item_params
      params.require(:line_item).permit(:product_id)
    end
end
