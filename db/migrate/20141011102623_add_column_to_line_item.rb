class AddColumnToLineItem < ActiveRecord::Migration
  def change
    add_column :line_items, :product_price, :int
  end
end
